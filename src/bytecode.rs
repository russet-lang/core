#[derive(Debug)]
pub enum Bytecode {
    SpawnAwait(usize),    //program_pointer
    IsAlive,              //Is Fiber alive?
    PushArgc(usize),      //args
    Continue,             //args
    Handle(usize, usize), //effectid, jump
    Yield,

    PushString(String),
    PushI32(i32),
    PushBool(bool),
    PushEffect(usize, usize), //argc

    Pop,

    Load(usize),
    Save(usize),

    Jump(usize), //where

    SkipFalse,
    SkipTrue,
    Halt,

    //Native effects
    Print,
    Read,
}
